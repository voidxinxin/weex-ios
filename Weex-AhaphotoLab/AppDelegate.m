//
//  AppDelegate.m
//  Weex-AhaphotoLab
//
//  Created by zhangxin on 2019/12/23.
//  Copyright © 2019 zhangxin. All rights reserved.
//

#import "AppDelegate.h"
#import <WeexSDK/WeexSDK.h>
#import "AhaImageLoadder.h"
#import "ViewController.h"
#import "WXEventModule.h"
#import "AhaTestModule.h"
@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    [self weexSetter];
    [self rooterViewSetter];
    return YES;
}
- (void)rooterViewSetter {
    self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.window.backgroundColor = [UIColor whiteColor];
    self.window.rootViewController = [[UINavigationController alloc] initWithRootViewController:[[ViewController alloc] init]];
    [self.window makeKeyAndVisible];
}
- (void)weexSetter {
    [WXAppConfiguration setAppGroup:@"zhangxin"];
    [WXAppConfiguration setAppName:@"Weex-AhaphotoLab"];
    [WXAppConfiguration setAppVersion:@"1.0"];
    //初始化SDK环境
    [WXSDKEngine initSDKEnvironment];
    //重写图片加载器
    [WXSDKEngine registerHandler:[AhaImageLoadder new] withProtocol:@protocol(WXImgLoaderProtocol)];
    [WXSDKEngine registerHandler:[[WXEventModule alloc] init] withProtocol:@protocol(WXEventModuleProtocol)];
    
    
    [WXSDKEngine registerComponent:@"select" withClass:NSClassFromString(@"WXSelectComponent")];
    [WXSDKEngine registerModule:@"event" withClass:[WXEventModule class]];
    
    //注册组件
    [WXSDKEngine registerComponent:@"ahaphoto" withClass:[AhaTestModule class]];
    
    //设置Log输出等级：调试环境默认为Debug，正式发布会自动关闭。
    [WXLog setLogLevel: WXLogLevelAll];
}

@end
