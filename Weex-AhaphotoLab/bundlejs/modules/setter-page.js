// { "framework": "Vue"} 

/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 33);
/******/ })
/************************************************************************/
/******/ ({

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = navigateToNextPage;
/**
 * 页面跳转路由
 */
var navigator = weex.requireModule('navigator');

var routes = [{ path: 'index', component: 'index', name: "首页" }, { path: 'error', component: 'error', name: "错误页面" }, { path: 'first-page', component: 'first-page', name: "第一页" }, { path: 'setter-page', component: 'setter-page', name: "设置页" }, { path: 'polycy-page', component: 'polycy-page', name: "隐私政策" }, { path: 'server-page', component: 'server-page', name: "服务" }, { path: 'feedback-page', component: 'feedback-page', name: "反馈" }];

function getRoute(component) {
    var name = "";
    var targets = routes.filter(function (route) {
        return route.component === component;
    });
    if (targets.length > 0) {
        name = targets.pop().path;
    } else {
        name = "error";
    }
    var arr = weex.config.bundleUrl.split("/");
    arr.pop();
    if (arr.includes("modules")) {
        arr.pop();
    }
    if (weex.config.env.platform === "Web") {
        arr.push(name + ".html");
        return arr.join("/");
    } else {
        arr.push(name + ".js");
        return arr.join("/");
    }
}

function navigateToNextPage(component) {
    navigator.push({
        url: getRoute(component),
        animated: "true"
    });
}
// function popToPreviousPage() {
//     navigator.pop({
//         animated: "true"
//      }, event => {
//          console.log('callback: ', event)
//       })
// }

/***/ }),

/***/ 33:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _setterPage = __webpack_require__(34);

var _setterPage2 = _interopRequireDefault(_setterPage);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

_setterPage2.default.el = '#root';
new Vue(_setterPage2.default);

/***/ }),

/***/ 34:
/***/ (function(module, exports, __webpack_require__) {

var __vue_exports__, __vue_options__
var __vue_styles__ = []

/* script */
__vue_exports__ = __webpack_require__(35)

/* template */
var __vue_template__ = __webpack_require__(36)
__vue_options__ = __vue_exports__ = __vue_exports__ || {}
if (
  typeof __vue_exports__.default === "object" ||
  typeof __vue_exports__.default === "function"
) {
if (Object.keys(__vue_exports__).some(function (key) { return key !== "default" && key !== "__esModule" })) {console.error("named exports are not supported in *.vue files.")}
__vue_options__ = __vue_exports__ = __vue_exports__.default
}
if (typeof __vue_options__ === "function") {
  __vue_options__ = __vue_options__.options
}
__vue_options__.__file = "/Users/zhangxin/Desktop/Weex相关/weex-demo/src/modules/setter-page.vue"
__vue_options__.render = __vue_template__.render
__vue_options__.staticRenderFns = __vue_template__.staticRenderFns
__vue_options__.style = __vue_options__.style || {}
__vue_styles__.forEach(function (module) {
  for (var name in module) {
    __vue_options__.style[name] = module[name]
  }
})
if (typeof __register_static_styles__ === "function") {
  __register_static_styles__(__vue_options__._scopeId, __vue_styles__)
}

module.exports = __vue_exports__


/***/ }),

/***/ 35:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _router = __webpack_require__(0);

var _router2 = _interopRequireDefault(_router);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
    data: function data() {
        return {
            dataArray: [{
                title: 'Feedback',
                image: 'https://i.loli.net/2019/12/25/VMzuly5BWgRHcIh.png'
            }, {
                title: 'Privacy Policy',
                image: 'https://i.loli.net/2019/12/25/VMzuly5BWgRHcIh.png'
            }, {
                title: 'Terms of Service',
                image: 'https://i.loli.net/2019/12/25/VMzuly5BWgRHcIh.png'
            }]
        };
    },

    methods: {
        popAction: function popAction() {
            var navigator = weex.requireModule('navigator');
            navigator.pop({
                animated: "true"
            }, function (event) {
                console.log('callback: ', event);
            });
        },
        cellClieck: function cellClieck(index) {
            if (index == 0) {
                (0, _router2.default)('feedback-page');
            } else if (index == 1) {
                (0, _router2.default)('polycy-page');
            } else if (index == 2) {
                (0, _router2.default)('server-page');
            }
        }
    }
}; //
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/***/ }),

/***/ 36:
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', [_c('div', {
    staticStyle: {
      backgroundColor: "rgb(30,30,30)",
      height: "166px",
      position: "relative",
      justifyContent: "center"
    }
  }, [_c('image', {
    staticStyle: {
      height: "88px",
      width: "88px",
      marginLeft: "20px"
    },
    attrs: {
      "src": "https://i.loli.net/2019/12/25/NPwgSBqucK5Avaj.png"
    },
    on: {
      "click": _vm.popAction
    }
  })]), _c('div', {
    staticStyle: {
      backgroundColor: "rgb(30,30,30)",
      height: "1230px"
    }
  }, _vm._l((_vm.dataArray), function(item, index) {
    return _c('div', {
      staticStyle: {
        height: "140px",
        justifyContent: "center"
      }
    }, [_c('div', {
      staticStyle: {
        flexDirection: "row"
      },
      on: {
        "click": function($event) {
          _vm.cellClieck(index)
        }
      }
    }, [_c('text', {
      staticStyle: {
        color: "white",
        fontSize: "40px",
        marginLeft: "30px"
      }
    }, [_vm._v(_vm._s(item.title))]), _c('div', {
      staticStyle: {
        flex: "1",
        alignItems: "center",
        flexDirection: "row-reverse"
      }
    }, [_c('image', {
      staticStyle: {
        height: "36px",
        width: "36px",
        marginRight: "30px"
      },
      attrs: {
        "src": item.image
      }
    })])])])
  }))])
},staticRenderFns: []}
module.exports.render._withStripped = true

/***/ })

/******/ });