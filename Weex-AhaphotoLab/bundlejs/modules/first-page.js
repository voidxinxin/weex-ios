// { "framework": "Vue"}

/******/ (function(modules) { // webpackBootstrap
/******/     // The module cache
/******/     var installedModules = {};
/******/
/******/     // The require function
/******/     function __webpack_require__(moduleId) {
/******/
/******/         // Check if module is in cache
/******/         if(installedModules[moduleId]) {
/******/             return installedModules[moduleId].exports;
/******/         }
/******/         // Create a new module (and put it into the cache)
/******/         var module = installedModules[moduleId] = {
/******/             i: moduleId,
/******/             l: false,
/******/             exports: {}
/******/         };
/******/
/******/         // Execute the module function
/******/         modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/         // Flag the module as loaded
/******/         module.l = true;
/******/
/******/         // Return the exports of the module
/******/         return module.exports;
/******/     }
/******/
/******/
/******/     // expose the modules object (__webpack_modules__)
/******/     __webpack_require__.m = modules;
/******/
/******/     // expose the module cache
/******/     __webpack_require__.c = installedModules;
/******/
/******/     // define getter function for harmony exports
/******/     __webpack_require__.d = function(exports, name, getter) {
/******/         if(!__webpack_require__.o(exports, name)) {
/******/             Object.defineProperty(exports, name, {
/******/                 configurable: false,
/******/                 enumerable: true,
/******/                 get: getter
/******/             });
/******/         }
/******/     };
/******/
/******/     // getDefaultExport function for compatibility with non-harmony modules
/******/     __webpack_require__.n = function(module) {
/******/         var getter = module && module.__esModule ?
/******/             function getDefault() { return module['default']; } :
/******/             function getModuleExports() { return module; };
/******/         __webpack_require__.d(getter, 'a', getter);
/******/         return getter;
/******/     };
/******/
/******/     // Object.prototype.hasOwnProperty.call
/******/     __webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/     // __webpack_public_path__
/******/     __webpack_require__.p = "";
/******/
/******/     // Load entry module and return exports
/******/     return __webpack_require__(__webpack_require__.s = 25);
/******/ })
/************************************************************************/
/******/ ({

/***/ 25:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _firstPage = __webpack_require__(4);

var _firstPage2 = _interopRequireDefault(_firstPage);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

_firstPage2.default.el = '#root';
new Vue(_firstPage2.default);

/***/ }),

/***/ 3:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = navigateToNextPage;
/**
 * 页面跳转路由
 */
var navigator = weex.requireModule('navigator');

var routes = [{ path: 'index', component: 'index', name: "首页" }, { path: 'error', component: 'error', name: "错误页面" }, { path: 'modules/first-page', component: 'first-page', name: "第一页" }, { path: 'modules/setter-page', component: 'setter-page', name: "设置页" }, { path: 'modules/polycy-page', component: 'polycy-page', name: "隐私政策" }, { path: 'modules/server-page', component: 'server-page', name: "服务" }, { path: 'modules/feedback-page', component: 'feedback-page', name: "反馈" }];

function getRoute(component) {
    var name = "";
    var targets = routes.filter(function (route) {
        return route.component === component;
    });
    if (targets.length > 0) {
        name = targets.pop().path;
    } else {
        name = "error";
    }
    var arr = weex.config.bundleUrl.split("/");
    arr.pop();
    if (arr.includes("modules")) {
        arr.pop();
    }
    if (weex.config.env.platform === "Web") {
        arr.push(name + ".html");
        return arr.join("/");
    } else {
        arr.push(name + ".js");
        return arr.join("/");
    }
}

function navigateToNextPage(component) {
    navigator.push({
        url: getRoute(component),
        animated: "true"
    });
}
// function popToPreviousPage() {
//     navigator.pop({
//         animated: "true"
//      }, event => {
//          console.log('callback: ', event)
//       })
// }

/***/ }),

/***/ 4:
/***/ (function(module, exports, __webpack_require__) {

var __vue_exports__, __vue_options__
var __vue_styles__ = []

/* styles */
__vue_styles__.push(__webpack_require__(5)
)

/* script */
__vue_exports__ = __webpack_require__(6)

/* template */
var __vue_template__ = __webpack_require__(7)
__vue_options__ = __vue_exports__ = __vue_exports__ || {}
if (
  typeof __vue_exports__.default === "object" ||
  typeof __vue_exports__.default === "function"
) {
if (Object.keys(__vue_exports__).some(function (key) { return key !== "default" && key !== "__esModule" })) {console.error("named exports are not supported in *.vue files.")}
__vue_options__ = __vue_exports__ = __vue_exports__.default
}
if (typeof __vue_options__ === "function") {
  __vue_options__ = __vue_options__.options
}
__vue_options__.__file = "/Users/zhangxin/Desktop/testFile/weex-demo/src/modules/first-page.vue"
__vue_options__.render = __vue_template__.render
__vue_options__.staticRenderFns = __vue_template__.staticRenderFns
__vue_options__._scopeId = "data-v-cfbd5ea6"
__vue_options__.style = __vue_options__.style || {}
__vue_styles__.forEach(function (module) {
  for (var name in module) {
    __vue_options__.style[name] = module[name]
  }
})
if (typeof __register_static_styles__ === "function") {
  __register_static_styles__(__vue_options__._scopeId, __vue_styles__)
}

module.exports = __vue_exports__


/***/ }),

/***/ 5:
/***/ (function(module, exports) {

module.exports = {
  "slider": {
    "width": "750",
    "height": "1250"
  },
  "image": {
    "width": "750",
    "height": "1250",
    "position": "absolute"
  },
  "frame": {
    "width": 100,
    "height": 100
  },
  "content": {
    "width": 100,
    "height": 100,
    "position": "absolute",
    "flexDirection": "row"
  }
}

/***/ }),

/***/ 6:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _router = __webpack_require__(3);

var _router2 = _interopRequireDefault(_router);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
  created: function created() {
    this.screenH = this.$getConfig().env.deviceHeight;
    this.screenW = this.$getConfig().env.deviceWidth;
  },
  data: function data() {
    return {
      screenH: 0,
      screenW: 0,
      fontSize: 100,
      title: 'scroll please!',
      imageList: [{ src: 'https://i.loli.net/2019/12/24/HZSjatmg2O6bNVz.jpg' }, { src: 'https://i.loli.net/2019/12/24/kQ6cYr3OzTCIq8P.jpg' }]
    };
  },

  methods: {
    setterClick: function setterClick() {
      //设置点击按钮
      (0, _router2.default)("setter-page");
    },
    bottomClick1: function bottomClick1() {
      this.title = 'click1';
      var arr = [{ src: 'https://i.loli.net/2019/12/24/HZSjatmg2O6bNVz.jpg' }, { src: 'https://i.loli.net/2019/12/24/kQ6cYr3OzTCIq8P.jpg' }];
      this.imageList = arr;
    },
    bottomClick2: function bottomClick2() {
      // this.title = 'click2'
      // var arr = [{src:'https://i.loli.net/2019/12/25/qES5gN9jKtfhdz2.jpg'},
      //            {src: 'https://i.loli.net/2019/12/25/utAzSxYKsnOQ79U.jpg'}
      //           ]
      // this.imageList = arr
      weex.requireModule("event").showAlert("hello Weex");
    },
    bottomClick3: function bottomClick3() {
      this.title = 'click3';
      var arr = [{ src: 'https://i.loli.net/2019/12/25/9EzKdMHJZVuNagB.jpg' }, { src: 'https://i.loli.net/2019/12/25/C4vMWtBlpfjo6On.jpg' }];
      this.imageList = arr;
    },
    bottomClick4: function bottomClick4() {
      this.title = 'click4';
      var arr = [{ src: 'https://i.loli.net/2019/12/25/UwTJEtYe1nWdNMK.jpg' }, { src: 'https://i.loli.net/2019/12/25/CnpU5Bv13O6wihc.jpg' }];
      this.imageList = arr;
    }
  },
  computed: {
    compClasses: function compClasses() {
      this.fontSize = 200;
    }
  }
}; //
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/***/ }),

/***/ 7:
/***/ (function(module, exports) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', [_c('slider', {
    staticClass: ["slider"],
    attrs: {
      "interval": "3000",
      "autoPlay": "false"
    }
  }, _vm._l((_vm.imageList), function(img, index) {
    return _c('div', {
      staticClass: ["frame"]
    }, [_c('div', {
      staticClass: ["content"]
    }, [_c('image', {
      staticClass: ["image"],
      attrs: {
        "resize": "cover",
        "src": img.src
      }
    }), _c('div', {
      staticStyle: {
        height: "120px",
        width: "120px",
        marginLeft: "35px",
        marginTop: "1050px",
        position: "relative"
      },
      on: {
        "click": _vm.bottomClick1
      }
    }), _c('div', {
      staticStyle: {
        height: "120px",
        width: "120px",
        marginLeft: "60px",
        marginTop: "1010px",
        position: "relative"
      },
      on: {
        "click": _vm.bottomClick2
      }
    }), _c('div', {
      staticStyle: {
        height: "120px",
        width: "120px",
        marginLeft: "60px",
        marginTop: "1010px",
        position: "relative"
      },
      on: {
        "click": _vm.bottomClick3
      }
    }), _c('div', {
      staticStyle: {
        height: "120px",
        width: "120px",
        marginLeft: "60px",
        marginTop: "1050px",
        position: "relative"
      },
      on: {
        "click": _vm.bottomClick4
      }
    }), _c('div', {
      staticStyle: {
        height: "120px",
        width: "120px",
        marginLeft: "600px",
        marginTop: "30px",
        position: "absolute"
      },
      on: {
        "click": _vm.setterClick
      }
    }), _c('text', {
      staticStyle: {
        backgroundColor: "green",
        position: "absolute"
      }
    }, [_vm._v(_vm._s(_vm.title))])])])
  }))])
},staticRenderFns: []}
module.exports.render._withStripped = true

/***/ })

/******/ });
