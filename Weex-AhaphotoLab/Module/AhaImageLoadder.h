//
//  AhaImageLoadder.h
//  Weex-AhaphotoLab
//
//  Created by zhangxin on 2019/12/25.
//  Copyright © 2019 zhangxin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <WeexSDK/WeexSDK.h>
NS_ASSUME_NONNULL_BEGIN
@interface AhaImageLoadder : NSObject<WXImgLoaderProtocol, WXModuleProtocol>
@end

NS_ASSUME_NONNULL_END
