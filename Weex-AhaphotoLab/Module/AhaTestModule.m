//
//  AhaTestModule.m
//  Weex-AhaphotoLab
//
//  Created by zhangxin on 2019/12/26.
//  Copyright © 2019 zhangxin. All rights reserved.
//

#import "AhaTestModule.h"

@implementation AhaTestModule

- (UIView *)loadView {
    UIButton *testView = [[UIButton alloc] init];
    [testView addTarget:self action:@selector(testAction) forControlEvents:UIControlEventTouchUpInside];
    return testView;
}
- (void)testAction {
    NSLog(@"test test test");
}
@end
