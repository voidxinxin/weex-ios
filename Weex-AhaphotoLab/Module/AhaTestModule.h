//
//  AhaTestModule.h
//  Weex-AhaphotoLab
//
//  Created by zhangxin on 2019/12/26.
//  Copyright © 2019 zhangxin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <WeexSDK/WXComponent.h>
NS_ASSUME_NONNULL_BEGIN

@interface AhaTestModule : WXComponent

@end

NS_ASSUME_NONNULL_END
