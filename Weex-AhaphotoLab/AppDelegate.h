//
//  AppDelegate.h
//  Weex-AhaphotoLab
//
//  Created by zhangxin on 2019/12/23.
//  Copyright © 2019 zhangxin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (nonatomic,strong) UIWindow *window;
@end

